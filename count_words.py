import collections
import json
import os
import pandas as pd

pt_words = [
    'vocês', 'você', 'está', 'estão', 'que', 'eu', 'você', 'ele', 'este', 'esse', 'isso', 'sua', 'e', 'mas', 'ou',
    'também', 'se', 'assim', 'como', 'porque', 'de', 'em', 'para', 'por', 'com', 'até', 'não', 'mais', 'muito', 'já',
    'quando', 'mesmo', 'depois', 'ainda', 'ser', 'ir', 'estar', 'ter', 'haver', 'fazer', 'dar', 'ficar', 'poder', 'ver',
    'bom', 'grande', 'melhor', 'pior', 'certo', 'último', 'próprio', 'coisa', 'casa', 'ano', 'dia', 'vez',
    'homem', 'senhor', 'senhora', 'moço', 'moça']
documents = os.listdir('data/')
word_list = []

for file_name in documents:
    print(file_name)
    with open(f'data/{file_name}', encoding="utf8") as json_file:
        data = json.load(json_file)
        data_to_processing = (
            data['body'].lower().replace(',', '').replace('.', '').replace('...', ' ').replace('…', ' '))
        word_list.extend(data_to_processing.split(' '))

word_list = [word for word in word_list if len(word) > 3]

for remove_word in pt_words:
    word_list = [s for s in word_list if s != remove_word]

print('Count')
json_words = collections.Counter(word_list)
print(json_words.most_common(100))
df = pd.DataFrame(json_words.most_common(100), columns=[
                  'word', 'number_count']).to_csv('count_words.csv', index=False)
